import { makeExecutableSchema } from 'graphql-tools';

import Tags from './connectors';
import { pubsub } from './subscriptions';

const typeDefs = [`

  type Account {
    id: Int
  }

  type Address {
    id: Int
    street: String
    county: String
    state: String
  }

  type Badge {
    _id: String!
    name: String!
    #rulesForEarning: ???
  }

  type Citation {
    id: Int
    user: User
    discription: String
  }

  type Database {
    id: Int
    name: String
  }

  type Department {
    id: Int
    users: [User]
  }

  type Event {
    id: Int
    name: String
    host: User
    limit: Int
    space: Space
#//  repeat: ???
    groceries: [Grocery]
  }

  type EquipmentType {
    id: Int
    name: String
    certificationNeeded: Boolean
  }

  type Equipment {
    id: Int 
    type: EquipmentType
  } 

  type Item {
    id: Int
    name: String
    work: Work
  }

  type File {
    id: Int
    path: String
  }

  type Fine {
    id: Int
    account: Account
    item: Item
  }

  type Grocery {
    id: Int
    name: String
  }

  type Hold {
#    // very similar to Fine
    id: Int
    account: Account
    item: Item
    location: Location
  }

  type Library {
    id: Int
    name: String
    state: String
    county: String
    locations: [Location]
    spaces: [Space]
    events: [Event]
  } 

  enum cardTypes {
    REGULAR
    OUT_OF_COUNTY
    STAFF
    MILITARY
    FRIENDS
    DONOR
  }

  type LibraryCard {
    id: Int
    type: cardTypes
  }

  type List {
    id: Int
    items: [Item]
  }

  type Location {
    id: Int
    name: String
    library: Library
    spaces: [Space]
  }

  type PhoneNumber {
    id: Int
    value: String
  }

  type Position {
    id: String!
    open: Boolean
  }

  type Request {
    id: Int
    work: Work
    item: Item
    amazon_id: String
  }

  type Reservation {
#    // very similar to Event
    id: Int
    user: User
    space: Space
    event: Event
  }

  type Role {
    id: Int
    admin: Boolean
  }

  type Solicitation {
    id: Int
    description: String
    attachment: File
  }

  type Space {
    id: Int
    name: String
  }

  type Tag {
    id: Int
    label: String
    type: String
  }

  type TagsPage {
    tags: [Tag]
    hasMore: Boolean
  }

  type User {
    id: Int
    firstName: String
    lastName: String
    address: Address
    phoneNumber: PhoneNumber
    connectedServices: [Vendor]
    isStaff: Boolean
    permissionsRole: Role
    authenticatedBy: User
    events: [Event]
    lists: [List]
  }

  type Vendor {
    id: Int
    name: String
  }

  type Website {
    _id: String!
    title: String!
    tags: [Tag]
  }

  type WebSession {
    _id: String!
    user: User
    #FIXME Date is not a default data type??
    #timeStart: Date
    #timeEnd: Date
  }

  type Work {
    items: [Item]
    name: String
  }

#########

  type Query {
    address: Address
    user: User
    work: Work
    
    hello: String
    ping(message:String!): String
    tags(type: String!): [Tag]
    tagsPage(page: Int!, size: Int!): TagsPage
    randomTag: Tag
    lastTag: Tag
  }

  type Mutation {
    addTag(type: String!, label: String!): Tag
  }

  type Subscription {
    tagAdded(type: String!): Tag
  }

  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }
`];

const resolvers = {
  Query: {
    hello(root, args, context) {
      return "Hello, World!";
    },
    ping(root, {message}, context) {
      return `Answering ${message}`;
    },
    tags(root, {type}, context){
      return Tags.getTags(type);
    },
    tagsPage(root, {page,size}, context) {
      return tags.getTagsPage(page, size);
    },
    randomTag(root, args, context) {
      return Tags.getRandomTag();
    },
    lastTag(root, args, context) {
      return Tags.getLastTag();
    }
  },
  Mutation: {
    addTag: async(root, {type, label}, context) => {
      console.log(`adding ${type} tag '${label}'`);
      const newTag = await Tags.addTag(type, label);
      pubsub.publish('tagAdded', newTag);
      return newTag;
    }
  },
  Subscription: {
    tagAdded(tag) {
      return tag;
    }
  }
};

const jsSchema = makeExecutableSchema({
  typeDefs,
  resolvers
});

export default jsSchema;
