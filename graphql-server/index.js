import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';

// subscriptions
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import {subscriptionManager } from './subscriptions';

import schema from './schema-library';

const PORT = 3020;
const WS_PORT = 3030;

var app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/graphql', graphqlExpress({ schema }));

app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
}));

app.listen(PORT, () => console.log(`App listening on port ${PORT}`));

// subscriptions
const websocketServer = createServer((request, response) => {
  response.writeHead(404);
  response.end();
});

websocketServer.listen(WS_PORT, () => console.log(`Websocket server listening on ${WS_PORT}`));

new SubscriptionServer({
  subscriptionManager,
}, websocketServer);
